function correctVerb(req, res, next) {
    if (req.query.type === undefined ||
        ["VERB", "SUBJECT", "ADJECTIVE"].indexOf(req.query.type) === -1)
    {
        console.log("Middleware correctVerb rejected, got type : " + req.query.type);
        next("Failed to parse the type of request");
        return;
    }

    next();
}

function properProviderData(req, res, next) {
    if (req.body.url === undefined || req.body.type === undefined) {
        console.log("Middleware properProviderData rejected, got body: " + JSON.stringify(req.body));
        next("Failed to parse the provider data, need url and type fields");
    }
    else if (["VERB", "SUBJECT", "ADJECTIVE"].indexOf(req.body.type) === -1)
    {
        console.log("Middleware properProviderData rejected, got type : " + req.body.type);
        next("Failed to parse the type of provider");
        return;
    }
    else next();
}

module.exports = {
    correctVerb,
    properProviderData
}