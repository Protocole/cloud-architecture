// Using dotenv, to require local .env file
require("dotenv").config();
const fetch = require("node-fetch");
/**
 *
 * @type {{VERB: [{url: string, type: string, user: string, active: boolean}], SUBJECT: [], ADJECTIVE: []}}
 */
const PROVIDERS = {
    VERB: [],
    SUBJECT: [],
    ADJECTIVE: []
}

class ProvidersController {
    static periodicTimer;

    /**
     * Fetching providers registered
     * @param req
     * @param res
     */
    static fetch(req, res) {
        console.log("Fetching " + req.query.type + ", number active: " + PROVIDERS[req.query.type].length);
        res
            .status(200)
            .json(PROVIDERS[req.query.type].filter(c => c.active))
            .end();
    }

    /**
     * Register a new provider in the memory
     * @param req
     * @param res
     */
    static register(req, res) {
        let providers = PROVIDERS[req.body.type];
        let provider = providers.find(c => c.url === req.body.url && !c.active);
        let user_provider = providers.find(c => c.url === req.body.url && c.user === req.body.user);

        if (req.body.user === undefined && provider)
        {
            provider.active = true;
            console.log("Updating active state for provider " + req.body.url);
        }
        else if (req.body.user && user_provider && !user_provider.active)
        {
            provider.active = true;
            console.log("Updating active state for provider " + req.body.user);
        }
        else if (!user_provider)
        {
            let body = req.body;
            body.active = true;
            PROVIDERS[req.body.type].push(body);
            console.log("Register new provider, type: " + req.body.type + ", url: " + req.body.url, " user: " + req.body.user);
            console.log("Stats, providers count: " + PROVIDERS[body.type].length + ", providers active: " + PROVIDERS[body.type].filter(c => c.active).length)
        }

        res.status(201)
            .json({success: true});
    }

    static startPeriodicCheck() {
        this.periodicTimer = setInterval(async () => await this.startProvidersCheck(), 6000);
    }

    static stopPeriodicCheck() {
        clearInterval(this.periodicTimer);
    }

    static async startProvidersCheck() {
        for (const verb of ["VERB", "ADJECTIVE", "SUBJECT"]) {
            await this.checkProviders(verb);
        }
    }

    static async checkProviders(verb) {
        let checked = [];

        for (const provider of PROVIDERS[verb]) {
            let active = await this.checkProvider(provider.url, provider.health ? provider.health : "/");

            if (active !== provider.active) {
                console.log("Provider " + provider.url + " (" + verb + ") is now " + (active ? "active" : "inactive") );
            }

            provider.active = active;

            if (!checked.find(c => c.url === provider.url)) {
                checked.push(provider);
            }
            else if (provider.user) checked.push(provider);
        }
    }

    static async checkProvider(url, healthUri) {
        return new Promise(res => {
            /**
             * I add timeout in case the target
             * got error and doesn't return anything
             */
            fetch(url + healthUri, {timeout: 2000})
                .then(result => {
                    if (parseInt(result.status) === 200 || parseInt(result.status) === 201)
                        res(true)
                    else res(false);
                })
                .catch(() => res(false));
        });
    }
}

module.exports = ProvidersController;