require("dotenv").config();
const express = require('express');
const router = express.Router();
const middleware = require("../middlewares/correctVerb");
const providersController = require("../controllers/providersController");

router.get('/', (req, res, next) => {
    res.json({message: "OK"})
        .end();
});

router.get("/providers", middleware.correctVerb, providersController.fetch);
router.put("/providers", middleware.properProviderData, providersController.register);

router.all("*", (req, res, next) => next("Route not found"));
router.use((err, req, res, next) => {
  res
      .status(400)
      .json({success: false, message: err})
      .end();
});
module.exports = router;
