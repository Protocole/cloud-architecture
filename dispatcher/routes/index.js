require('dotenv').config();
var express = require('express');
var router = express.Router();
const fetch = require("node-fetch");
const providerController = require("../controllers/providerController");

const providerUri = process.env.PROVIDER_HOST;
const providerPort = process.env.PROVIDER_PORT;
const providerFullUri = `${!process.env.PROVIDER_SSL ? "https://" : "http://"}${process.env.PROVIDER_HOST}:${process.env.PROVIDER_PORT}`;

async function fetchFromProvider(path) {
    return await fetch(providerFullUri + path);
}

/* GET home page. */
router.all('/', anyProvider, providerController.fetch);

function anyProvider(req, res, next) {
  if (!providerController.any())
  {
    res.status(500)
        .json({message: "No provider available!"})
        .end();
    return;
  }

  next();
}

module.exports = router;
