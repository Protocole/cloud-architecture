require("dotenv").config();
const fetch = require("node-fetch");
const registers = require("../registers.json");

const PROVIDERS = {
    "VERB": [],
    "ADJECTIVE": [],
    "SUBJECT": []
};

class registerController {
    static updateTimer;

    static periodicUpdate() {
        this.updateTimer = setInterval(() => {
            registers.forEach(register => this.fetch(register))
        }, 12000);
    }

    static stopUpdate() {
        clearInterval(this.updateTimer);
    }

    static async fetch(register) {
        try {
            for (const verb of ["VERB", "ADJECTIVE", "SUBJECT"]) {
                let result = await fetch(register + "/providers?type=" + verb, {timeout: 2000});
                result = await result.json()
                PROVIDERS[verb].push(...result);
            }
        }
        catch (e) {
            console.error(e);
        }
    }

}

module.exports = {
    registerController,
    PROVIDERS
};