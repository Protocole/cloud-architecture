const fetch = require("node-fetch");
const PROVIDERS = require("./registerController").PROVIDERS;

class providerController {
    static any() {
        return PROVIDERS["VERB"].length > 0 && PROVIDERS["SUBJECT"].length > 0 && PROVIDERS["ADJECTIVE"].length > 0;
    }

    static fetch(req, res) {
        Promise.all([
            providerController.race(PROVIDERS["VERB"], "verb"),
            providerController.race(PROVIDERS["SUBJECT"], "subject"),
            providerController.race(PROVIDERS["ADJECTIVE"], "adjective"),
        ]).then(result => {
            let [subject, verb, adjective] = result;
            res.render('index', { title: "Welcome to dispatcher", subject: subject, verb: verb, adjective: adjective});

        })
            .catch(err => {
                console.error(err);
                res.status(500)
                    .json({message: "Provider is not ready"})
                    .end();
            });
    }

    static race(providers, verb)
    {
        /**
         * using race, we fetch from every provider
         * and if one of the provider is down at the time
         * of fetch, there will be always more to backup
         */
        return Promise.race(providers.map(c => {
            return new Promise(async (res) => {
                try {
                    let result = await fetch(c.url + "/" + verb)
                    result = await result.json();
                    res(result);
                }
                catch (e) {

                }
            });
        }));
    }
}

module.exports = providerController;