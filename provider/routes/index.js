require("dotenv").config();
const express = require('express');
const router = express.Router();
const wordController = require("../controllers/wordController");

router.get('/', (req, res, next) => {
    res.json({message: "OK"})
        .end();
});

router.get('/health', (req, res, next) => {
    res.json({message: "OK", instanceId: process.env.INSTANCE_ID, appId: process.env.APP_ID})
        .end();
});

router.get('/subject', wordController.subject);
router.get('/adjective', wordController.adjective);
router.get('/verb', wordController.verb);

router.all("*", (req, res, next) => {
  res.json({message: "Route not found"})
      .end();
});

router.use((err, req, res, next) => {
    res
        .json({message: "An error occured: " + err})
        .end();
});
module.exports = router;
