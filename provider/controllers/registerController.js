require('dotenv').config();
const fetch = require('node-fetch')

class registerController {
    static async registerProvider(register) {
        if (register === undefined)
        {
            console.log("No register referenced, I'm sad :(");
            return;
        }

        console.log("Register to " + register);
        console.log("My app link is " + "https://" + process.env.APP_ID + ".cleverapps.io");

        let failed = false;
        for (const item of ["VERB", "ADJECTIVE", "SUBJECT"]) {
            if (!await this.registerWord(register, item))
            {
                failed = true;
            }
            else console.log("Provider is now registered for " + item)
        }

        if (failed)
        {
            console.log("Failed to register myself with , I'm sad! I'll retry in 5 seconds...");
            setTimeout(() => registerController.registerProvider(register), 5000);
        }
    }

    static async registerWord(register, word) {
        let app_id = process.env.APP_URL;

        let body = {
            url: app_id,
            type: word,
            user: "alexandre " + process.env.CC_PRETTY_INSTANCE_NAME
        };

        try {
            let register_result = await fetch(register + "/providers",
                {
                    method: "PUT",
                    body: JSON.stringify(body),
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                });

            if (register_result.status !== 201) return false;
            else return true;
        }
        catch (e) {
            console.log(e.message);
            console.log("Failed to register " + word)
            return false;
        }
    }
}

module.exports = registerController;