const subjects = ["micro-service", "car", "pen", "smartphone", "hJune", "Twitch", "Youtube"];
const verbs = ["is", "have", "enroll", "destroy", "create", "deploy", "load", "kill", "ride"];
const adjectives = ["a carjacked", "a trump", "a red", "a blue", "a golden", "a silver", "a dead"];

function random(list) {
    return list[Math.floor(Math.random() * list.length)];
}

class WordController {
    static verb(req, res) {
        console.log("Fetching verb, verbs number: " + verbs.length);
        res.json({value: random(verbs), user: process.env.INSTANCE_ID})
            .end();
    }

    static adjective(req, res) {
        console.log("Fetching adjective, adjectives number: " + adjectives.length);
        res.json({value: random(adjectives), user: process.env.INSTANCE_ID})
            .end();
    }

    static subject(req, res) {
        console.log("Fetching subject, subjects number: " + subjects.length);
        res.json({value: random(subjects), user: process.env.INSTANCE_ID})
            .end();
    }
}

module.exports = WordController;