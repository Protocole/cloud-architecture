# Cloud Sample

A simple architecture of a scalable system between three entities. This project is coming from a 
school work given by @fteychene.

The aim is to create a [cadavre exquis](https://en.wikipedia.org/wiki/Exquisite_corpse) using words
with a constant communications between three entities: 
dispatcher (render the words), provider (give words) and register (listing active providers). 
These applications are following the [12 factors](https://12factor.net/).

The topology is the following one:

![](https://i.imgur.com/PvEkv1E.png)

* Dispatchers are applications accessible by any client, it shows 3 words (a subject, an adjective and a verb)
* Providers are simple API to give a random word depending on of the type needed (subject, adjective, verb)
* Registers are active workers to determine if any provider became inactive or back active

**Everything is powered by Express in NodeJS and has been deployed using CleverCloud.**
*These applications are using environment variable names relative to the PaaS used.*

## Dispatcher

The dispatcher is the "front-end" part, it fetched words using providers API. 
The providers the dispatcher uses are taken from registers, the list of providers is dynamic. 
Registers used are available in the file `registers.json`.

The list of providers is fetched every 12 seconds.

## Provider

The provider is a backend API returning random words based on a specified type (subject, adjective or verb). 
When launching it registers itself to registers available in the file `registers.json`.

When a provider is registering itself to a register, it shoud contain the following body:

```json
{
    url: {Provider's URL},
    type: {VERB|ADJECTIVE|SUBJECT},
    user: {Pretty name of provider}
}
```

## Register

The register is the central piece of this sample. It saves every providers in its memory. 
The register is called every time the dispatcher wants to refresh its list of providers.

The register will (almost) always return active providers as every 3 seconds it checks if any provider 
got offline or back online. So, unless you got bad luck you'll have working providers.

## Routes

| App | Verb | Path | Description |
| --- | ---- | ---- | ----------- |
| Dispatcher |
| | GET | / | Fetch cadavre exquis words |
| Provider |
| | GET | /providers?type={VERB\|ADJECTIVE\|SUBJECT} | Get a random word based on the type given
| | GET | / | Returns OK |
| | GET | /health | Health check (give the instance ID & app ID) |
| Register |
| | POST | /providers | Register a new provider (see [provider's body](#Provider))